[![Crowdin](https://badges.crowdin.net/ygo-omega/localized.svg)](https://crowdin.com/project/ygo-omega)

# Omega Locale

Localization files for Omega

# Omega in your Language

## Contribute
Help translate Omega in your langauge, Join https://crowdin.com/project/ygo-omega

## Request a new Language
If you dont see your Language [here](urlhttps://crowdin.com/project/ygo-omega), then suggest it @ [https://forum.duelistsunite.org](https://forum.duelistsunite.org/c/omega/suggestions/50)

**Requesting a new language, you need to be able to maintain it**

## Become an Locale Proofreader
Apply to become a Proofreader @ [https://forum.duelistsunite.org](https://forum.duelistsunite.org/tags/c/omega/projects/12/translation)
